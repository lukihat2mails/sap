from flask import Flask, request
import numpy as np
import pandas as pd
from scipy.fftpack import fft
from scipy.optimize import fminbound
from threading import Thread
import multiprocessing

# Definition of global variables
totalSteps = 0
x = pd.read_csv('data1.csv')

# Algorithm to calculate steps from given data set using Fast Fourier Transformation
def calculate_steps(data):

    fs = 100  # sampling frequency
    N = 320;  # window size fft 20
    ts = 1.25  # duration of the sliding window
    res = fs / N  # resolution of the fft
    ls = int(ts * fs)  # length of the sliding windows

    i = 0
    step_count = 0
    global totalSteps

    while i + N < len(data):

        idx = np.argmax(np.mean(abs(data.iloc[i:i + N, 1:4])))

        S = 2 * abs(fft(data.iloc[i:i + N, idx + 1].to_numpy()))
        w0 = np.mean(S[0:2])
        wc = np.mean(S[2:7])

        coefficients = np.polyfit([1, 2, 3, 4, 5], S[2:7], 4)
        f = lambda x: -np.polyval(coefficients, x)
        maximum = fminbound(f, 1, 5)

        if wc > w0 and wc > 10:
            fw = res * (maximum + 1)
            c = ts * fw
            step_count = step_count + c

        i = i + ls

    totalSteps += step_count

app = Flask(__name__)

@app.route('/steps', methods=['GET'])
def receive_http_call():

    if request.method == 'GET':

        splits = np.array_split(x, 3)   # Split into 3 evenly sized "Slave"-Datasets
        results = []
        for y in splits:
            results.append(Thread(target=calculate_steps, args=(y,)))

        # Start the threads
        counter = 0
        for y in splits:
            results[counter].start()
            counter = counter + 1

        # Join the threads
        counter = 0
        for y in splits:
            results[counter].join()
            counter = counter + 1

        # Return the calculated steps after joining all threads
        return 'Total Steps: ' + str(totalSteps)

if __name__ == '__main__':
    app.run()








